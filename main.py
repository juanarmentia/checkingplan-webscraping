#!/usr/bin/env python3
import logging

from bs4 import BeautifulSoup
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import date, timedelta
from omegaconf import OmegaConf

logging.basicConfig(level=logging.DEBUG)

config = OmegaConf.load('config.yaml')

driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))

logging.debug('Charging web')
driver.get(config.url)
page = driver.page_source
soup = BeautifulSoup(page, 'html.parser')

logging.debug('Parsing web to search user and pass inputs')
user_input = driver.find_element(by=By.XPATH, value='//input[@type="text"]')
pass_input = driver.find_element(by=By.XPATH, value='//input[@type="password"]')

logging.debug('Setting user and pass')
user_input.send_keys(config.credentials.user)
pass_input.send_keys(config.credentials.password)

today = date.today()
weekday = today.weekday()
if weekday in range(5, 7):
    day = today - timedelta(days=weekday)
else:
    day = today - timedelta(days=weekday+7)

n_days = 6

logging.debug('Clicking login button')
WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.CLASS_NAME, 'dxbButtonSys'))).click()

logging.debug('For each day fill time data and click submit')
for i in range(1, n_days):
    date_text = day.strftime("%Y-%m-%d")
    if date_text not in config.free_days:
        if config.is_summer:
            time_start = config.time.summer.start
            time_end = config.time.summer.end
        else:
            time_start = config.time.regular.start
            time_end = config.time.regular.end
        url = f'https://tecnalia.checkingplan.com/wfp_portal_empleados.aspx?Id=tecnalia&fecfhjman=' + date_text + \
              f'&horaentrada={time_start}&horasalida={time_end}&turno=1'
        driver.get(url)

        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//div[@title="Salida"]'))).click()

    day = day + timedelta(days=1)

logging.debug('Closing web')
driver.quit()
